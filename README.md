# font-online
在线字库，通过 [UNPKG CDN](https://www.unpkg.com/) 引入字体文件，减少服务器带宽压力。

目前，提供**思源黑体**、**思源宋体**和**阿里巴巴普惠体3.0**。

# 引入
font-online提供了三种引入字体的方法，直接引入、CSS引入、模块化引入。

## 直接引入
项目根目录fonts文件夹下提供了所有字体的otf文件，可以UNPKG的规则直接引入字体文件。
```
https://www.unpkg.com/font-online/fonts/SourceHanSans/SourceHanSans-Normal.otf
```

## CSS引入
项目根目录css文件夹下提供了Web引入CSS文件。可以直接在浏览器中引入，或通过Webpack引入。
```html
<!-- 思源黑体相关class，已过时，不建议使用 -->
<style href="https://www.unpkg.com/font-online/css/source-han-sans.css"></style>
<!-- 思源黑体@font-face -->
<style href="https://www.unpkg.com/font-online/css/source-han-sans.online.css"></style>
<!-- 字体font-family相关class -->
<style href="https://www.unpkg.com/font-online/css/font-family.css"></style>
<!-- 字重font-weight相关class -->
<style href="https://www.unpkg.com/font-online/css/font-weight.css"></style>
```

```javascript
// 思源黑体相关class，已过时，不建议使用
import "font-online/css/source-han-sans.css";
// 思源黑体@font-face
import "font-online/css/source-han-sans.online.css";
// 字体font-family相关class
import "font-online/css/font-family.css";
// 字重font-weight相关class
import "font-online/css/font-weight.css";
```

## 模块化引入
Webpack模块化引入。
```javascript
import font from "font-online";
// 引入字体定义
font.load.sourceHanSans.online(); // 引入在线字体
font.load.sourceHanSans.offline(); // 引入离线字体
// 引入字体class
font.family();
// 引入字重class
font.weight();
```

# CSS class使用
CSS class分为字体class和字重class。

## 字体class
字体class中定义了``font-family``。
- ``font-source-han-sans``: 思源黑体，如果字体不存在，会依次退化到``Arial, Helvetica, sans-serif``。
- ``font-source-han-serif``: 思源宋体。
- ``font-alibaba-pu-hui-ti-3``: 阿里巴巴普惠体3.0。

## 字重class
字体class中定义了``font-weight``。
- ``font-100``: ``100``字重。
- ``font-200``: ``200``字重。
- ``font-300``: ``300``字重。
- ``font-400``: ``400``字重。
- ``font-500``: ``500``字重。
- ``font-600``: ``600``字重。
- ``font-700``: ``700``字重。
- ``font-800``: ``800``字重。
- ``font-900``: ``900``字重。
- ``font-1000``: ``1000``字重。
- ``font-default``: ``normal``字重。
- ``font-normal``: ``normal``字重。
- ``font-bold``: ``bold``字重。