const sourceHanSans = {
    online: () => {
        import("./css/source-han-sans.online.css");
        import("./css/source-han-sans.css");
        import("./css/font-family.css");
    },
    offline: () => {
        import("./css/source-han-sans.offline.css");
        import("./css/source-han-sans.css");
        import("./css/font-family.css");
    }
};

const sourceHanSerif = {
    online: () => {
        import("./css/source-han-serif.online.css");
        import("./css/source-han-serif.css");
        import("./css/font-family.css");
    },
    offline: () => {
        import("./css/source-han-serif.offline.css");
        import("./css/source-han-serif.css");
        import("./css/font-family.css");
    }
};

const alibabaPuHuiTi3 = {
    online: () => {
        import("./css/alibaba-pu-hui-ti-3.online.css");
        import("./css/font-family.css");
    },
    offline: () => {
        import("./css/alibaba-pu-hui-ti-3.offline.css");
        import("./css/font-family.css");
    }
};

const fontLoad = {
    sourceHanSans,
    sourceHanSerif,
    alibabaPuHuiTi3
};

const fontFamily = () => {
    import("./css/font-family.css");
};

const fontWeight = () => {
    import("./css/font-weight.css");
};

export default {
    load: fontLoad,
    family: fontFamily,
    weight: fontWeight,
    // 旧版
    sourceHanSans,
    sourceHanSerif
};